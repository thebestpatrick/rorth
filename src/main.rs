extern crate itertools;
extern crate rustyline;

use std::{collections::HashMap, ops::{Index, Add, Mul, Sub, Div, BitXor}};

use itertools::Itertools;
use rustyline::DefaultEditor;

const WORD_PARSE_ERROR: &str = "Encountered unrecognized word";
const UNDERFLOW_ERROR: &str = "Handle stack underflow";
const ARITHMETIC_OPERAND_ERROR: &str = "Handle arithmetic operand type errors";

#[derive(Debug, Clone, Copy)]
enum Instruction {
    ShowStack,
    Show,

    Drop,
    Dup,
    Swap,
    Over,

    Add,
    Sub,
    Mul,
    Div,
    Xor,
}

struct Dictionary {
    definitions: HashMap<String, Vec<Instruction>>,
}

impl Default for Dictionary {
    fn default() -> Self {
        use Instruction::*;
        let mut dict = HashMap::new();
        dict.insert(".s".to_string(), vec![ShowStack]);
        dict.insert(".".to_string(), vec![Show]);
        dict.insert("drop".to_string(), vec![Drop]);
        dict.insert("dup".to_string(), vec![Dup]);
        dict.insert("swap".to_string(), vec![Swap]);
        dict.insert("over".to_string(), vec![Over]);
        dict.insert("+".to_string(), vec![Add]);
        dict.insert("-".to_string(), vec![Sub]);
        dict.insert("*".to_string(), vec![Mul]);
        dict.insert("/".to_string(), vec![Div]);
        dict.insert("xor".to_string(), vec![Xor]);
        Self { definitions: dict }
    }
}

impl Index<&String> for Dictionary {
    type Output = Vec<Instruction>;

    fn index(&self, index: &String) -> &Self::Output {
        self.definitions
            .get(index)
            .expect("Failing here means the word doesn't exist.")
    }
}

struct Parser {
    input: Vec<char>,
    cursor: usize,
}

impl Parser {
    pub fn new(input: String) -> Parser {
        Parser {
            input: input.chars().collect_vec(),
            cursor: 0,
        }
    }

    fn skip_whitespace(&mut self) {
        while !self.at_eoi() && self.input[self.cursor].is_whitespace() {
            self.cursor += 1;
        }
    }

    pub fn consume_word(&mut self) -> Option<String> {
        self.skip_whitespace();

        let start_idx = self.cursor;
        while !self.at_eoi() && !self.input[self.cursor].is_whitespace() {
            self.cursor += 1;
        }

        if start_idx == self.cursor {
            None
        } else {
            Some(self.input[start_idx..self.cursor].iter().collect())
        }
    }

    fn at_eoi(&self) -> bool {
        self.cursor == self.input.len()
    }
}

#[derive(Default)]
enum Mode {
    #[default]
    Interpreting,
    Compiling,
}

#[derive(Debug, Clone, Copy)]
enum Value {
    Bool(bool),
    Integer(usize),
    Float(f64),
}

macro_rules! value_from {
    ($ty:ty, $name:ident) => {
        impl From<$ty> for Value {
            fn from(value: $ty) -> Self {
                use Value::*;
                $name(value)
            }
        }
    };
}

value_from!(bool, Bool);
value_from!(usize, Integer);
value_from!(f64, Float);

impl TryFrom<String> for Value {
    type Error = ();
    fn try_from(value: String) -> Result<Value, ()> {
        let parse_bool = value.parse::<bool>().map(Value::from).map_err(|_| ());
        let parse_int = |_| value.parse::<usize>().map(Value::from).map_err(|_| ());
        let parse_float = |_| value.parse::<f64>().map(Value::from).map_err(|_| ());
        let success = parse_bool.or_else(parse_int).or_else(parse_float);
        success
    }
}

macro_rules! impl_op {
    ($ty:ident, $fn:ident) => {
        impl $ty for Value {
            type Output = Option<Value>;

            fn $fn(self, rhs: Self) -> Self::Output {
                match (self, rhs) {
                    (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer($ty::$fn(lhs, rhs))),
                    (Value::Integer(lhs), Value::Float(rhs))  => Some(Value::Float($ty::$fn(lhs as f64, rhs))),
                    (Value::Float(lhs), Value::Integer(rhs))  => Some(Value::Float($ty::$fn(lhs, rhs as f64))),
                    (Value::Float(lhs), Value::Float(rhs)) => Some(Value::Float($ty::$fn(lhs, rhs))),
                    _ => None,
                }
            }
        }
    };
}

impl_op!(Add, add);
impl_op!(Sub, sub);
impl_op!(Mul, mul);
impl_op!(Div, div);

impl Value {
    fn boolify(self) -> Value {
        match self {
            Value::Bool(_) => self,
            Value::Integer(i) => Value::from(i != 0),
            Value::Float(i) => Value::from(i != 0.0),
        }
    }
}

impl BitXor for Value {
    type Output = Option<Value>;

    fn bitxor(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Value::Bool(lhs), Value::Bool(rhs)) => Some(Value::from(lhs ^ rhs)),
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::from(lhs ^ rhs)),
            (Value::Float(_), __) => None,
            (__, Value::Float(_)) => None,
            (Value::Bool(_), Value::Integer(_)) => todo!(),
            (Value::Integer(_), Value::Bool(_)) => todo!(),
            (_, _) => None,
        }
    }
}

struct Interpreter {
    dictionary: Dictionary,
    mode: Mode,
    stack: Vec<Value>,
}

impl Default for Interpreter {
    fn default() -> Self {
        Self {
            dictionary: Default::default(),
            mode: Default::default(),
            stack: Vec::default(),
        }
    }
}

impl Interpreter {
    pub fn interpret(&mut self, input: String) {
        use Mode::*;

        let mut parser = Parser::new(input);

        match self.mode {
            Interpreting => {
                while let Some(word) = parser.consume_word() {
                    if self.dictionary.definitions.contains_key(&word) {
                        let tokens = self.dictionary[&word].clone();
                        for token in tokens {
                            self.execute(token);
                        }
                    } else {
                        let v = Value::try_from(word).expect(WORD_PARSE_ERROR);
                        self.stack.push(v);
                    }
                }
            }
            Compiling => todo!(),
        }
    }

    fn execute(&mut self, token: Instruction) {
        use Instruction::*;
        match token {
            ShowStack => {
                println!("{:?}", self.stack);
            }
            Show => println!("{:?}", self.stack.pop().expect(UNDERFLOW_ERROR)),
            Drop => _ = self.stack.pop(),
            Dup => {
                let tos = self
                    .stack
                    .get(self.stack.len() - 1)
                    .expect(UNDERFLOW_ERROR)
                    .clone();
                self.stack.push(tos);
            }
            Swap => {
                let v0 = self
                    .stack
                    .get(self.stack.len() - 1)
                    .expect(UNDERFLOW_ERROR)
                    .clone();
                let v1 = self
                    .stack
                    .get(self.stack.len() - 2)
                    .expect(UNDERFLOW_ERROR)
                    .clone();
                let len = self.stack.len();
                self.stack[len - 2] = v0;
                self.stack[len - 1] = v1;
            }
            Over => {
                let v1 = self
                    .stack
                    .get(self.stack.len() - 2)
                    .expect(UNDERFLOW_ERROR)
                    .clone();
                self.stack.push(v1);
            }
            Add => {
                let v0 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);
                let v1 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);

                let result = (v0 + v1).expect(ARITHMETIC_OPERAND_ERROR);
                self.stack.push(result)
            }
            Sub => {
                let v0 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);
                let v1 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);

                let result = (v1 - v0).expect(ARITHMETIC_OPERAND_ERROR);
                self.stack.push(result)
            }
            Mul => {
                let v0 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);
                let v1 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);

                let result = (v0 * v1).expect(ARITHMETIC_OPERAND_ERROR);
                self.stack.push(result)
            }
            Div => {
                let v0 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);
                let v1 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);

                let result = (v1 / v0).expect(ARITHMETIC_OPERAND_ERROR);
                self.stack.push(result)
            }
            Xor => {
                let v0 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);
                let v1 = self
                    .stack
                    .pop()
                    .expect(UNDERFLOW_ERROR);

                let result = (v1 ^ v0).expect(ARITHMETIC_OPERAND_ERROR);
                self.stack.push(result)
            }
        }
    }
}

fn main() -> rustyline::Result<()> {
    let mut interpreter = Interpreter::default();
    let mut rl = DefaultEditor::new()?;
    while let Ok(line) = rl.readline(">> ") {
        interpreter.interpret(line);
    }
    Ok(())
}
